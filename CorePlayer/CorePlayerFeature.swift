//
//  CorePlayerFeature.swift
//  CorePlayer
//
//  Created by flexih on 4/20/15.
//  Copyright (c) 2015 flexih. All rights reserved.
//

import AVFoundation

@objc public enum State: Int {
    case None
    case AssetReady
    case ItemReady
    case PlayReady
    case End
    case Failed
    case Error
    case Stop
}

@objc public enum Error: Int {
    case URLError = -1000
    case Error
}

@objc public protocol CorePlayerFeature: NSObjectProtocol {
    
    var scaleFill: Bool { get set }
    #if os(iOS)
    var allowAirPlay: Bool { get set }
    #endif
    
    func playURL(URL: NSURL)
    func playURLs(mediaArray: Array<RemoteMedia>)
    func appendURLs(mediaArray: Array<RemoteMedia>)
    func appendURL(URL: NSURL)
    
    var avplayer: AVPlayer? { get }
    var moduleManager: ModuleManager { get }
    
    var view: ContentView { get }
    //func playerView.() -> UXView
    
    func currentMediaItem() -> RemoteMedia?
    func duration() -> NSTimeInterval
    func played() -> NSTimeInterval
    func state() -> State
    func presentationSize() -> CGSize
    
    func isPending() -> Bool
    func isSeeking() -> Bool
    
    /**
     Seeking URL.from
    */
    func isSeekHeading() -> Bool
    func isPlaying() -> Bool
    func isFinish() -> Bool
    
    /**
     User pause
    */
    func isDirectPause() -> Bool
    #if os(iOS)
    func isAirplaying() -> Bool
    #endif
    
    func play()
    func pause()
    
    /**
     User pause
    */
    func dpause()
    
    func beginSeek(time: NSTimeInterval)
    func seekTo(time: NSTimeInterval)
    func endSeek(time: NSTimeInterval)
    func seekToEnd()
}
