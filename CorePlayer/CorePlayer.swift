//
//  CorePlayer.swift
//  CorePlayer
//
//  Created by flexih on 4/21/15.
//  Copyright (c) 2015 flexih. All rights reserved.
//

import AVFoundation
#if os(iOS)
    import UIKit
#else
    import AppKit
#endif

let kTracksKey           = "tracks"
let kStatusKey           = "status"
let kPlaybackKeepUpKey   = "playbackLikelyToKeepUp"
let kPresentationSizeKey = "presentationSize"
let kRateKey             = "rate"
let kDurationKey         = "duration"
let kLoadedKey           = "loadedTimeRanges"
let kPlayableKey         = "playable"
let kCurrentItemKey      = "currentItem"
let kTimedMetadataKey    = "currentItem.timedMetadata"

public class CorePlayer: NSObject {
    
    struct Keyobserver {
        var keep = false
        var rate = false
        var duration = false
        var status = false
        var item = false
        var meta = false
        var loaded = false
        var airplay = false
        var presentation = false
    }
    
    struct PlayerState {
        var state: State = .None
        var lastplay = false
        var seeking = false
        var play = false
        var pending = false
        var airplay = false
        var dpause = false
        var readyplay = false
        var seekhead = false
    }
    
    private var keyobserver = Keyobserver()
    var playerState = PlayerState()
    
    public private(set) var moduleManager: ModuleManager
    public private(set) var view: ContentView
    private(set) var playerView: PlayerView
    
    var playerItem: PlayerItem?
    var playerAsset: AVURLAsset?
    private(set) var backPlayer: Player?
    private(set) var backView: PlayerView?
    var playedObserver: AnyObject?
    var mediaArray: Array<RemoteMedia> = []
    var currentMediaIndex: Int = 0
    public var avplayer: AVPlayer? {
        get {
            if let player = player {
                return player as AVPlayer
            } else {
                return nil
            }
        }
    }
    var player: Player? {
        willSet {
            if player != nil {
                deregisterPlayerEvent()
            }
        }
        
        didSet {
            playerView.playerLayer().player = player
        }
    }
    
    #if os(iOS)
    var interruption: Interruption
    #endif
    
    public init(manager: ModuleManager) {
        view = ContentView()
        playerView = PlayerView()
        moduleManager = manager
        
        #if os(iOS)
            interruption = Interruption()
            playerView.clipsToBounds = true
            playerView.backgroundColor = UIColor.blackColor()
        #else
            view.acceptsTouchEvents = true
        #endif
        
        super.init()
        
        moduleManager.moduleDelegate = self
        view.layoutManager = self
        view.addSubview(playerView)
        
        #if os(iOS)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "appBecomeActive:", name: UIApplicationDidBecomeActiveNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "appResignActive:", name: UIApplicationWillResignActiveNotification, object: nil)
        #endif
    }
    
    public override convenience init() {
        self.init(manager: ModuleManager())
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
        
        moduleManager.deinitModule()
        
        view.removeFromSuperview()
    }
    
    func observePlayed() {
        var span: NSTimeInterval = 1
        
        if durationSpan() > 0 {
            span = playerItem!.cduration() / NSTimeInterval(durationSpan())
        }
        
        if (span < 0.5) {
            span = 0.5
        } else if (span > 1) {
            span = 1
        }
        
        unobservePlayed()
        
        playedObserver = player!.addPeriodicTimeObserverForInterval(CMTimeMakeWithSeconds(Float64(span), Int32(NSEC_PER_SEC)), queue: dispatch_get_main_queue(), usingBlock: { [weak self] time in
            if let strongSelf = self {
                strongSelf.moduleManager.played(strongSelf.played())
            }
        })
    }
    
    func unobservePlayed() {
        if playedObserver != nil {
            player!.removeTimeObserver(playedObserver!)
            playedObserver = nil
        }
    }
    
    func playable() -> NSTimeInterval {
        if let loadedRanges = playerItem?.loadedTimeRanges {
            if loadedRanges.count > 0 {
                let range: CMTimeRange = loadedRanges[0].CMTimeRangeValue
                let rangeTime = CMTimeAdd(range.start, range.duration)
                let playable = CMTimeGetSeconds(rangeTime)
                return playable
            }
        }
        return 0
    }
    
    public func durationSpan() -> Int {
        return 1
    }
    
    public func handlePlayEnd() {
        
    }
    
    public func handleError(error: Error) {
        moduleManager.error(error)
    }
    
    public func stop() {
        if playerState.state == .PlayReady  ||
           playerState.state == .ItemReady  ||
           playerState.state == .AssetReady ||
           playerState.state == .Error {
            
            if playerState.state != .Error {
                playerState.state = .Stop
            }
            
            stopPlaybackSession()
            
        } else if playerState.state == .None {
            moduleManager.cancelPlay()
        }
        
        if playerState.state != .Stop {
            playerState.state = .Stop
        }
    }
    
    func setdpause(dpause: Bool) {
        playerState.dpause = dpause
    }
    
    func startPause() {
        moduleManager.willPause()
    }
    
    func stopPause() {
        moduleManager.endPause()
    }
    
    func startPending() {
        moduleManager.willPend()
    }
    
    func stopPending() {
        moduleManager.endPend()
    }
    
    func startLoading() {
        moduleManager.willSection(currentMediaItem()!)
        moduleManager.willPlay()
    }
    
    func stopLoading() {
        moduleManager.startSection(currentMediaItem()!)
        
        if currentMediaIndex == 0 {
            moduleManager.startPlay()
            
            #if os(iOS)
            interruption.observeInterruption(self)
            #endif
        }
    }
    
    #if os(iOS)
        
    func appResignActive(notification: NSNotification) {
        if let player = player {
            playerState.lastplay = player.isPlaying()
            
            if !isAirplaying() {
                player.pause()
            }
        }
        
        moduleManager.appResign()
    }
    
    func appBecomeActive(notification: NSNotification) {
        if playerState.readyplay {
            playerState.lastplay = playerState.readyplay
            playerState.readyplay = false
        }
        
        if playerState.dpause {
            playerState.lastplay = false
        }
        
        if playerState.lastplay {
            playerState.lastplay = false
            player?.play()
        }
        
        moduleManager.appActive()
    }
    
    #endif
    
    func stopPlaybackSession() {
        /*remove observe play event first*/
        deregisterRate()
        
        player?.pause()
        player?.cancelPendingPrerolls()
        playerItem?.cancelPendingSeeks()
        playerAsset?.cancelLoading()
        playerView.playerLayer().player = nil
        
        backPlayer?.pause()
        backPlayer?.cancelPendingPrerolls()
        backPlayer?.currentItem?.asset.cancelLoading()
        backView?.playerLayer().player = nil
        
        unobservePlayed()
        
        deregisterPlayerEvent()
        deregisterPlayerItemEvent()
        
        if playerState.state == .PlayReady ||
           playerState.state == .End       ||
           playerState.state == .Failed    ||
           playerState.state == .Stop {
                moduleManager.endSection(currentMediaItem()!)
                moduleManager.endPlayCode(playerState.state)
                #if os(iOS)
                interruption.unobserver()
                #endif
        }
    
        playerState.play = false
        playerState.dpause = false
        playerState.seeking = false
        playerState.seekhead = false
        playerState.pending = false
        playerState.readyplay = false
    
        currentMediaIndex = 0
        mediaArray.removeAll(keepCapacity: true)
        playerAsset = nil
        playerItem = nil
        player = nil
        backPlayer = nil
        backView = nil
    }
    
    func playEnded(end: Bool) {
        playerState.state = !end ? .Failed : .End
        
        if currentMediaIndex + 1 >= mediaArray.count || playerState.state == .Failed {
            stopPlaybackSession()
            
            if playerState.state == .Failed {
                handleError(.Error)
            } else {
                handlePlayEnd()
            }
            
        } else {
            deregisterRate()
            player?.pause()
            playerItem?.cancelPendingSeeks()
            playerAsset?.cancelLoading()
            unobservePlayed()
            deregisterPlayerEvent()
            deregisterPlayerItemEvent()
            
            playerState.play = false
            playerState.dpause = false
            playerState.seeking = false
            playerState.seekhead = false
            playerState.pending = false
            playerState.readyplay = false
            
            playerState.state = .End
            moduleManager.endSection(currentMediaItem()!)
            
            currentMediaIndex += 1
            playerState.state = .ItemReady
            
            moduleManager.willSection(currentMediaItem()!)
            
            playerItem = backPlayer?.currentItem as? PlayerItem
            playerAsset = playerItem?.asset as? AVURLAsset
            
            registerPlayerItemEvent()
            
            let superview: UXView! = playerView.superview
            
            playerView.removeFromSuperview()
            
            if superview == view {
                #if os(iOS)
                    view.insertSubview(backView!, atIndex:0)
                #else
                    view.addSubview(backView!, positioned:.Below, relativeTo:nil)
                #endif
            } else {
                superview.addSubview(backView!)
            }
            
            backView!.frame = playerView.frame
            playerView = backView!
            playerView.gravity = scaleFill ? .Fill : .Aspect
            player = backPlayer
            #if os(iOS)
                player!.allowsExternalPlayback = allowAirPlay
            #endif
            backPlayer = nil
            backView = nil
            
            registerPlayerEvent()
            
            if playerItem?.status == .ReadyToPlay {
                readyToPlay()
            } else {
                startPending()
            }
            
            loadNext()
        }
    }
    
    func playerItemDidFailed(notification: NSNotification) {
        playEnded(false)
    }
    
    func playerItemDidReachEnd(notification: NSNotification) {
        playEnded(player!.isPlayToEnd())
    }
    
    func readyToPlay() {
        playerState.state = .PlayReady
        stopLoading()
        
        if currentMediaItem()!.from > 0.0 {
            playerState.seekhead = true
            
            player?.seekToTime(CMTimeMakeWithSeconds(Float64(currentMediaItem()!.from), Int32(NSEC_PER_SEC)), accurate: true, completion: { [weak self] finished in
                if let strongSelf = self {
                    strongSelf.playerState.seekhead = false
                    
                    if finished {
                        strongSelf.player?.play()
                    }
                }
            })
            
            currentMediaItem()!.from = 0.0
        }
        
        var active = true
        #if os(iOS)
            active = UIApplication.sharedApplication().applicationState == .Active || playerState.airplay
        #endif
            
        if playerState.seekhead || playerState.dpause || !active {
            playerState.readyplay = !playerState.dpause
        } else if !playerState.seekhead {
            player?.play()
        }
            
        if playerItem!.cduration() > 0 {
            moduleManager.durationAvailable(duration())
        }
    }
    
    func registerPlayerItemEvent() {
        let options = NSKeyValueObservingOptions([.Old, .New])
        
        playerItem?.addObserver(self, forKeyPath:kStatusKey, options:options, context: UnsafeMutablePointer<Void>(unsafeAddressOf(kStatusKey)))
        keyobserver.status = true
        
        playerItem?.addObserver(self, forKeyPath:kPlaybackKeepUpKey, options:options, context:nil)
        keyobserver.keep = true
        
        playerItem?.addObserver(self, forKeyPath:kPresentationSizeKey, options:options, context:nil)
        keyobserver.presentation = true
        
        playerItem?.addObserver(self, forKeyPath:kDurationKey, options:options, context:nil)
        keyobserver.duration = true
        
        playerItem?.addObserver(self, forKeyPath:kLoadedKey, options:options, context:nil)
        keyobserver.loaded = true
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"playerItemDidReachEnd:", name:AVPlayerItemDidPlayToEndTimeNotification, object:playerItem)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"playerItemDidFailed:", name:AVPlayerItemFailedToPlayToEndTimeNotification, object:playerItem)
    }
    
    func deregisterPlayerItemEvent() {
        if keyobserver.status {
            playerItem?.removeObserver(self, forKeyPath:kStatusKey)
            keyobserver.status = false
        }
        
        if keyobserver.keep {
            playerItem?.removeObserver(self, forKeyPath:kPlaybackKeepUpKey)
            keyobserver.keep = false
        }
        
        if keyobserver.presentation {
            playerItem?.removeObserver(self, forKeyPath:kPresentationSizeKey)
            keyobserver.presentation = false
        }
        
        if keyobserver.duration {
            playerItem?.removeObserver(self, forKeyPath:kDurationKey)
            keyobserver.duration = false
        }
        
        if keyobserver.loaded {
            playerItem?.removeObserver(self, forKeyPath:kLoadedKey)
            keyobserver.loaded = false
        }
        
        #if os(iOS)
            if keyobserver.airplay {
                player?.unlistenAirPlayState(self)
                keyobserver.airplay = false
            }
        #endif
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name:AVPlayerItemDidPlayToEndTimeNotification, object:playerItem)
        NSNotificationCenter.defaultCenter().removeObserver(self, name:AVPlayerItemFailedToPlayToEndTimeNotification, object:playerItem)
    }
    
    func registerPlayerEvent() {
        let options = NSKeyValueObservingOptions([.Old, .New])
        
        player?.addObserver(self, forKeyPath:kRateKey, options:options, context:nil)
        keyobserver.rate = true
        
        player?.addObserver(self, forKeyPath:kCurrentItemKey, options:options, context:nil)
        keyobserver.item = true
        
        player?.addObserver(self, forKeyPath:kTimedMetadataKey, options:options, context:nil)
        keyobserver.meta = true
        
        #if os(iOS)
            keyobserver.airplay = player?.listenAirPlayState(self) ?? false
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "audioSessionRouteChange:", name: AVAudioSessionRouteChangeNotification, object: nil)
        #endif
    }
    
    func audioSessionRouteChange(notification: NSNotification) {
        
    }
    
    func deregisterPlayerEvent() {
        if keyobserver.rate {
            player?.removeObserver(self, forKeyPath:kRateKey)
            keyobserver.rate = false
        }
        
        if keyobserver.item {
            player?.removeObserver(self, forKeyPath:kCurrentItemKey)
            keyobserver.item = false
        }
        
        if keyobserver.meta {
            player?.removeObserver(self, forKeyPath:kTimedMetadataKey)
            keyobserver.meta = false
        }
        
        #if os(iOS)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: AVAudioSessionRouteChangeNotification, object: nil)
        #endif
    }
    
    func deregisterRate() {
        if keyobserver.rate {
            player?.removeObserver(self, forKeyPath:kRateKey)
            keyobserver.rate = false
        }
    }
    
    func loadNext() {
        if backPlayer != nil {
            return
        }
        
        if currentMediaIndex + 1 < mediaArray.count {
            let cpu = mediaArray[currentMediaIndex + 1]
            let asset = AVURLAsset(remoteMedia: cpu)
            let playerItem = PlayerItem(asset: asset)
            
            backPlayer = Player(playerItem: playerItem)
            backPlayer!.actionAtItemEnd = .Pause
            #if os(iOS)
                backPlayer!.allowsExternalPlayback = false
            #endif
            backView = PlayerView()
            backView!.playerLayer().player = backPlayer
        }
    }

    override public func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if keyPath == kPlaybackKeepUpKey {
            if playerState.state == .AssetReady ||
               playerState.state == .ItemReady {
                return
            }

            let keep = (change?[NSKeyValueChangeNewKey] as! NSNumber).boolValue
            
            if keep {
                stopPending()
            } else {
                startPending()
            }
            
            playerState.pending = !keep
            
        } else if keyPath == kRateKey {
            let rate = (change?[NSKeyValueChangeNewKey] as! NSNumber).floatValue
            let isPlay = Player.isRatePlaying(rate)
            
            if isPlay {
                if playedObserver == nil {
                    observePlayed()
                }
                
                if !playerState.play {
                    stopPause()
                }
                
                if playerState.readyplay {
                    playerState.readyplay = false
                }
                
            } else {
                if playedObserver != nil {
                    unobservePlayed()
                }
                
                if playerState.play {
                    startPause()
                }
            }
            
            playerState.play = isPlay
            
        } else if keyPath == kLoadedKey {
            let loadedRanges = playerItem!.loadedTimeRanges
            if loadedRanges.count > 0 {
                moduleManager.playable(playable())
            }
            
        } else if keyPath == kStatusKey {
            let status = AVPlayerStatus(rawValue: (change?[NSKeyValueChangeNewKey] as! NSNumber).integerValue)!
            switch (status) {
            case .ReadyToPlay:
                if playerState.state.rawValue >= State.PlayReady.rawValue {
                    #if os(iOS)
                        let applicationState = UIApplication.sharedApplication().applicationState
                        if ((applicationState == .Active || playerState.airplay) && !playerState.dpause) {
                            player!.play()
                        }
                    #else
                        player!.play()
                    #endif
                    return
                }
                
                readyToPlay()
                
            case .Failed, .Unknown:
                if playerState.state == .PlayReady {
                    playerState.state = .Failed
                } else {
                    playerState.state = .Error
                }
                
                stopPlaybackSession()
                handleError(.Error)
            }
            
        } else if keyPath == kDurationKey {
            moduleManager.durationAvailable(duration())
            
        } else if keyPath == kCurrentItemKey {
            
        } else if keyPath == kTimedMetadataKey {
            
        } else if keyPath == kPresentationSizeKey {
            #if os(iOS)
                moduleManager.presentationSize((change?[NSKeyValueChangeNewKey] as! NSValue).CGSizeValue())
            #else
                moduleManager.presentationSize((change?[NSKeyValueChangeNewKey] as! NSValue).sizeValue)
            #endif
        } else {
            #if os(iOS)
            if keyPath == player!.airPlayObserverKey() {
                playerState.airplay = (change?[NSKeyValueChangeNewKey] as! NSNumber).boolValue
                moduleManager.airplayShift(playerState.airplay)
                return
            }
            #endif
            super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
        }
    }
}

extension CorePlayer: ContentLayoutManager {

    func contentsLayout(view: UXView) {
        let v: UXView = view.subviews[0]

        if v.isMemberOfClass(PlayerView) {
            v.frame = view.bounds
        }
        
        moduleManager.layoutView()
    }
    
    #if os(iOS)
    
    func contentsTouchesBegan(touches: Set<NSObject>, withEvent event: UIEvent?) {
        moduleManager.touchesBegan(touches, withEvent: event)
    }
    
    func contentsTouchesMoved(touches: Set<NSObject>, withEvent event: UIEvent?) {
        moduleManager.touchesMoved(touches, withEvent: event)
    }
    
    func contentsTouchesEnded(touches: Set<NSObject>, withEvent event: UIEvent?) {
        moduleManager.touchesEnded(touches, withEvent: event)
    }
    
    func contentsTouchesCancelled(touches: Set<NSObject>!, withEvent event: UIEvent?) {
        moduleManager.touchesCancelled(touches, withEvent: event)
    }
    
    #else
    
    func contentsMouseDown(theEvent: NSEvent) {
        moduleManager.mouseDown(theEvent)
    }
    
    func contentsRightMouseDown(theEvent: NSEvent) {
        moduleManager.rightMouseDown(theEvent)
    }
    
    func contentsOtherMouseDown(theEvent: NSEvent) {
        moduleManager.otherMouseDown(theEvent)
    }
    
    func contentsMouseUp(theEvent: NSEvent) {
        moduleManager.mouseUp(theEvent)
    }
    
    func contentsRightMouseUp(theEvent: NSEvent) {
        moduleManager.rightMouseUp(theEvent)
    }
    
    func contentsOtherMouseUp(theEvent: NSEvent) {
        moduleManager.otherMouseUp(theEvent)
    }
    
    func contentsMouseMoved(theEvent: NSEvent) {
        moduleManager.mouseMoved(theEvent)
    }
    
    func contentsMouseDragged(theEvent: NSEvent) {
        moduleManager.mouseDragged(theEvent)
    }
    
    func contentsScrollWheel(theEvent: NSEvent) {
        moduleManager.scrollWheel(theEvent)
    }
    
    func contentsRightMouseDragged(theEvent: NSEvent) {
        moduleManager.rightMouseDragged(theEvent)
    }
    
    func contentsOtherMouseDragged(theEvent: NSEvent) {
        moduleManager.otherMouseDragged(theEvent)
    }
    
    func contentsMouseEntered(theEvent: NSEvent) {
        moduleManager.mouseEntered(theEvent)
    }
    
    func contentsMouseExited(theEvent: NSEvent) {
        moduleManager.mouseExited(theEvent)
    }
    
    func contentsKeyDown(theEvent: NSEvent) {
        moduleManager.keyDown(theEvent)
    }
    
    func contentsKeyUp(theEvent: NSEvent) {
        moduleManager.keyUp(theEvent)
    }
    
    #endif
}

#if os(iOS)

extension CorePlayer: InterruptionDelegate {
    
    func interrupt(reason: InterruptionReason) {
        moduleManager.interrupt(reason)
    }
}
    
#endif

extension CorePlayer: CorePlayerFeature {
    
    public var scaleFill: Bool {
        
        get {
            return playerView.gravity == .Fill
        }
        
        set {
            playerView.gravity = scaleFill ? .Fill : .Aspect
        }
    }
    
    #if os(iOS)
    public var allowAirPlay: Bool {
        
        get {
            if player != nil {
                return player!.allowsExternalPlayback
            } else {
                return false
            }
        }
        
        set {
            player?.allowsExternalPlayback = newValue
        }
    }
    #endif

    public func playURL(URL: NSURL) {
        playURLs([RemoteMedia(URL: URL)])
    }
    
    public func playURLs(mediaArray: Array<RemoteMedia>) {
        if playerState.state == .PlayReady ||
           playerState.state == .ItemReady ||
           playerState.state == .AssetReady {
            stopPlaybackSession()
        }
        
        if mediaArray.count == 0 {
            handleError(.URLError)
            return
        }
        
        currentMediaIndex = 0
        self.mediaArray = mediaArray
        playerState.state = .ItemReady
        playerAsset = AVURLAsset(remoteMedia: currentMediaItem()!)
        playerItem = PlayerItem(asset: playerAsset!)
        
        registerPlayerItemEvent()
        
        player = Player(playerItem: playerItem! as AVPlayerItem)
        player!.actionAtItemEnd = .Pause
        #if os(iOS)
        player!.allowsExternalPlayback = allowAirPlay
        #endif
        
        registerPlayerEvent()
        
        startLoading()
        
        loadNext()
    }

    public func appendURL(URL: NSURL) {
        appendURLs([RemoteMedia(URL: URL)])
    }
    
    public func appendURLs(mediaArray: Array<RemoteMedia>) {
        
        self.mediaArray.appendContentsOf(mediaArray)
        
        loadNext()
    }

    public func currentMediaItem() -> RemoteMedia? {
        if mediaArray.isEmpty {
            return nil
        }
        
        return mediaArray[currentMediaIndex]
    }

    public func duration() -> NSTimeInterval {
        if let playerItem = playerItem {
            return playerItem.cduration()
        }
        
        return 0
    }
    
    public func played() -> NSTimeInterval {
        if playerState.state == .End {
            return duration()
        }
        
        if let player = player {
            var rlt = player.ccurrentTime()
            let d = playerItem?.cduration() ?? 0
            
            if rlt > d {
                rlt = d
            }
            
            return rlt
        }
        
        return 0
    }

    public func state() -> State {
        return playerState.state
    }

    public func presentationSize() -> CGSize {
        return player?.currentItem?.presentationSize ?? CGSizeZero
    }

    public func isPending() -> Bool {
        return playerState.pending
    }
    
    public func isSeekHeading() -> Bool {
        return playerState.seekhead
    }
    
    public func isSeeking() -> Bool {
        return playerState.seeking
    }
    
    public func isPlaying() -> Bool {
        return player?.isPlaying() ?? false
    }

    public func isDirectPause() -> Bool {
        return playerState.dpause
    }
    
    #if os(iOS)
    public func isAirplaying() -> Bool {
        return player?.externalPlaybackActive ?? false
    }
    #endif
    
    public func isFinish() -> Bool {
        return playerState.state == .End
    }

    public func play() {
        playerState.dpause = false
        player?.play()
    }
    
    public func pause() {
        player?.pause()
    }
    
    public func dpause() {
        playerState.dpause = true
        pause()
    }

    public func beginSeek(time: NSTimeInterval) {
        if playerState.state != .PlayReady {
            return
        }
        
        unobservePlayed()
        moduleManager.startSeek(time)
    }
    
    public func seekTo(time: NSTimeInterval) {
        if playerState.state != .PlayReady {
            return
        }
        
        playerItem?.cancelPendingSeeks()
        
        playerState.seeking = true
        
        player?.seekToTime(CMTimeMakeWithSeconds(Float64(time), Int32(NSEC_PER_SEC)), accurate: true, completion: { [weak self] finished in
            if let strongSelf = self {
                strongSelf.playerState.seeking = false
                
                if finished {
                    strongSelf.observePlayed()
                }
            }
        })
        
        moduleManager.seekTo(time)
    }
    
    public func endSeek(time: NSTimeInterval) {
        if playerState.state != .PlayReady {
            return
        }
        
        if !playerState.seeking && playedObserver == nil {
            observePlayed()
        }
        
        var isend = false
        let end = playerItem!.cduration()
        
        if (isfinite(end) && fabs(time - end) < 0.5) {
            if (playerState.seeking) {
                playerItem!.cancelPendingSeeks()
            }
            
            isend = true
            moduleManager.endSeek(time, isEnd:isend)
            seekToEnd()
        } else {
            moduleManager.endSeek(time, isEnd:isend)
        }
    }
    
    public func seekToEnd() {
        playEnded(true)
    }
}
