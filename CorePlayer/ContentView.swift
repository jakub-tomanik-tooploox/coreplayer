//
//  ContentView.swift
//  CorePlayer
//
//  Created by flexih on 4/21/15.
//  Copyright (c) 2015 flexih. All rights reserved.
//

#if os(iOS)
    import UIKit
#else
    import AppKit
#endif

protocol ContentLayoutManager: NSObjectProtocol {
    
    func contentsLayout(view: UXView)
    
    #if os(iOS)
    func contentsTouchesBegan(touches: Set<NSObject>, withEvent event: UIEvent?)
    func contentsTouchesMoved(touches: Set<NSObject>, withEvent event: UIEvent?)
    func contentsTouchesEnded(touches: Set<NSObject>, withEvent event: UIEvent?)
    func contentsTouchesCancelled(touches: Set<NSObject>!, withEvent event: UIEvent?)
    #else
    func contentsMouseDown(theEvent: NSEvent)
    func contentsRightMouseDown(theEvent: NSEvent)
    func contentsOtherMouseDown(theEvent: NSEvent)
    func contentsMouseUp(theEvent: NSEvent)
    func contentsRightMouseUp(theEvent: NSEvent)
    func contentsOtherMouseUp(theEvent: NSEvent)
    func contentsMouseMoved(theEvent: NSEvent)
    func contentsMouseDragged(theEvent: NSEvent)
    func contentsScrollWheel(theEvent: NSEvent)
    func contentsRightMouseDragged(theEvent: NSEvent)
    func contentsOtherMouseDragged(theEvent: NSEvent)
    func contentsMouseEntered(theEvent: NSEvent)
    func contentsMouseExited(theEvent: NSEvent)
    func contentsKeyDown(theEvent: NSEvent)
    func contentsKeyUp(theEvent: NSEvent)
    #endif
}

public class ContentView: UXView {
    
    weak var layoutManager: ContentLayoutManager?
    
    #if os(iOS)
    override public func layoutSubviews() {
        super.layoutSubviews()
        layoutManager?.contentsLayout(self)
    }

    public override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        layoutManager?.contentsTouchesBegan(touches, withEvent: event)
    }

    public override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        layoutManager?.contentsTouchesMoved(touches, withEvent: event)
    }

    public override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        layoutManager?.contentsTouchesEnded(touches, withEvent: event)
    }
    
    public override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        layoutManager?.contentsTouchesCancelled(touches, withEvent: event)
    }
    
    #else
    public override func resizeSubviewsWithOldSize(oldSize: NSSize) {
        super.resizeSubviewsWithOldSize(oldSize)
        layoutManager?.contentsLayout(self)
    }
    
    public override func mouseDown(theEvent: NSEvent) {
        super.mouseDown(theEvent)
        layoutManager?.contentsMouseDown(theEvent)
    }
    
    public override func rightMouseDown(theEvent: NSEvent) {
        super.rightMouseDown(theEvent)
        layoutManager?.contentsRightMouseDown(theEvent)
    }
    
    public override func otherMouseDown(theEvent: NSEvent) {
        super.otherMouseDown(theEvent)
        layoutManager?.contentsOtherMouseDown(theEvent)
    }
    
    public override func mouseUp(theEvent: NSEvent) {
        super.mouseUp(theEvent)
        layoutManager?.contentsMouseUp(theEvent)
    }
    
    public override func rightMouseUp(theEvent: NSEvent) {
        super.rightMouseUp(theEvent)
        layoutManager?.contentsRightMouseUp(theEvent)
    }
    
    public override func otherMouseUp(theEvent: NSEvent) {
        super.otherMouseUp(theEvent)
        layoutManager?.contentsOtherMouseUp(theEvent)
    }
    
    public override func mouseMoved(theEvent: NSEvent) {
        super.mouseMoved(theEvent)
        layoutManager?.contentsMouseMoved(theEvent)
    }
    
    public override func mouseDragged(theEvent: NSEvent) {
        super.mouseDragged(theEvent)
        layoutManager?.contentsMouseDragged(theEvent)
    }
    
    public override func scrollWheel(theEvent: NSEvent) {
        super.scrollWheel(theEvent)
        layoutManager?.contentsScrollWheel(theEvent)
    }
    
    public override func rightMouseDragged(theEvent: NSEvent) {
        super.rightMouseDragged(theEvent)
        layoutManager?.contentsRightMouseDragged(theEvent)
    }
    
    public override func otherMouseDragged(theEvent: NSEvent) {
        super.otherMouseDragged(theEvent)
        layoutManager?.contentsOtherMouseDragged(theEvent)
    }
    
    public override func mouseEntered(theEvent: NSEvent) {
        super.mouseEntered(theEvent)
        layoutManager?.contentsMouseEntered(theEvent)
    }
    
    public override func mouseExited(theEvent: NSEvent) {
        super.mouseExited(theEvent)
        layoutManager?.contentsMouseExited(theEvent)
    }
    
    public override func keyDown(theEvent: NSEvent) {
        super.keyDown(theEvent)
        layoutManager?.contentsKeyDown(theEvent)
    }
    
    public override func keyUp(theEvent: NSEvent) {
        super.keyUp(theEvent)
        layoutManager?.contentsKeyUp(theEvent)
    }
    #endif
}


